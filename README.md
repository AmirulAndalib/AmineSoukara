<h1 align="center"> DAMIEN SOUKARA! </h1> 
<p align="center">
<img src="https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https://github.com/AmineSoukara/&title=Profile%20Views">
</p>
<a href="https://AmineSoukara.me"><img alt="LinkinSkye" src="https://i.imgur.com/07aPB5A.jpg"></a>

- 👾 My Name is Amine Soukara, You Can Call Me DAMIEN
- 🏫 I'm an English student at Sidi Mohamed Ben Abdellah

<details>
  <summary>Some Interesting Facts About Me!</summary>
  
  - 😄 Ta Qlwa Gha Kmel Tri9k Bla Mt9ra 
  
  - 🎮 Makla N3as, Github, Games (PUBG & COD)

  - 🎵 Listening To Music While Coding, And Developing Useful Codes.
  
  - 📚 Reading Novels, Action and Adventure, Autobiography & Biography, Comics, Detective and Mystery, Fantasy, Historical Fiction, Sci-Fi, History books

  - 🥅 2021 Goals: Learn JavaScript, Go, C++ etc.

  - ⚡ Fun fact: I'm Single
</details>

![RUN](https://github.com/AmineSoukara/AmineSoukara/raw/master/dino.gif)
 
<hr>
<h3 align="center">Languages & Tools :</h3>
<p align="center"><i>As a student, i always want to study more and more. Don't hesitate to give me your knowledge!</i></p>
<p align="center">
<a href="https://www.gnu.org/software/bash/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a>
<a href="https://www.android.com/" target="_blank"> <img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/android/android.png" alt="android" width="40" height="40"/> </a>
<a href="https://.com" target="_blank"> <img src="https://img.icons8.com/color/48/000000/visual-studio-code-2019.png" alt="Visual Studio Code" width="40p" height="40"/> </a>
<a href="https://www.docker.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a>
<a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a>
<a href="https://cloud.google.com" target="_blank"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="37" height="37"/> </a>
<a href="https://heroku.com" target="_blank"> <img src="https://www.vectorlogo.zone/logos/heroku/heroku-icon.svg" alt="Heroku" width="37" height="37"/> </a>
<a href="https://css.com" target="_blank"> <img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png" alt="css" width="40" height="40"/> </a>
<a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a>
<a href="https://nodejs.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40"/> </a>
<a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a>
<a href="https://php.com/" target="_blank"> <img src="https://raw.githubusercontent.com/jmnote/z-icons/master/svg/php.svg" alt="php" width="40" height="40"/> </a>
<a href="https://www.postgresql.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> </a>
 </p>
</p>
<p align="center">
</p>

<a href="https://AmineSoukara.me"><img src="https://github-stats-alpha.vercel.app/api/?username=AmineSoukara&cc=fff&tc=000&ic=000" alt="DamienSoukara"></a>
<!--
[![Amine Streak](https://github-readme-streak-stats.herokuapp.com/?user=AmineSoukara&currStreakNum=ff0000&fire=red&sideLabels=00000)](https://github.com/AmineSoukara)
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=AmineSoukara&show_icons=true&hide_border=true&layout=compact&langs_count=8)](https://github.com/AmineSoukara)
[![willianrod's wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=AmineSoukara)](https://github.com/AmineSoukara)
-->
<hr>
<h3 align="center">Social Media</h3>
<p align="center"><i>Let's connect and chat! Open to anyone on Earth under the Sun and Moon.</i></p>
<p align="center">
<a href="https://bit.ly/AmineSoukaraIG" alt="Instagram"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/instagram.ico"></a>
<a href="https://bit.ly/AmineSoukaraTme" alt="Telegram"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/telegram.ico"></a>
<a href="https://bit.ly/AmineSoukaraTwitter" alt="Twitter"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/twitter.ico"></a>
<a href="https://bit.ly/AmineSoukaraFacebook" alt="Facebook"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/facebook.ico"></a>
<a href="https://bit.ly/AmineSoukaraSnap" alt="Snap"><img height="30" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/snap.ico"></a>
<a href="https://bit.ly/AmineSoukaraWhtsp" alt="WhatsApp"><img height="35" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/whtsp.ico"></a>
<a href="https://bit.ly/AmineSoukaraGit" alt="Github"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/github.ico"></a>
<a href="https://bit.ly/AmineSoukaraYoutube" alt="Youtube"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/youtube.ico"></a>
<a href="mailto:AmineSoukara@gmail.com" alt="Gmail"><img height="33" src="https://raw.githubusercontent.com/AmineSoukara/AmineSoukara.github.io/master/img/logo/gmail.ico"></a>
</p>

<hr>
<h2 align="center">Music :</h2>
<p align="center"><b><i>"Don't Spy On Me 😅🤓"</i></b></p>
<p align="center">
<img src="https://spotify-github-profile.vercel.app/api/view?uid=215m7o2db7shzyoxpnsajilpy&cover_image=true&theme=default">
<!--
<img src="https://spotify-recently-played-readme.vercel.app/api?user=215m7o2db7shzyoxpnsajilpy&count=3">
-->
</p>

### Projects and Dev Stuffs:

<details>	
  <summary><b>⚡ Github Stats</b></summary>

<img height="180em" src="https://github-readme-stats.vercel.app/api?username=AmineSoukara&show_icons=true&hide_border=true&&count_private=true&include_all_commits=true" />
<img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=AmineSoukara&show_icons=true&hide_border=true&layout=compact&langs_count=8"/>
</details>

<details>	
  <summary><b>☄️ Github Streaks</b></summary>

<img height="180em" src="https://github-readme-streak-stats.herokuapp.com/?user=AmineSoukara&hide_border=true" />
</details>

<details>
  <summary><b>🧑‍🚀 Open Source Projects</b></summary>

  <br />
  <table>
    <thead align="center">
      <tr border: none;>
        <td><b>💻 Projects</b></td>
        <td><b>🌟 Stars</b></td>
        <td><b>🍴 Forks</b></td>
        <td><b>👨‍💻 Language</b></td>
      </tr>
    </thead>
    <tbody>
      <tr>
	      <td><a href="https://github.com/AmineSoukara"><b>🚀 test</b></a></td>
        <td><img alt="Stars" src="https://img.shields.io/github/stars/iampavangandhi/Gitwar?style=flat-square&labelColor=343b41"/></td>
        <td><img alt="Forks" src="https://img.shields.io/github/forks/iampavangandhi/Gitwar?style=flat-square&labelColor=343b41"/></td>
        <td><img alt="Language" src="https://img.shields.io/github/languages/top/iampavangandhi/Gitwar?style=flat-square"/></td>
      </tr>
      <tr>
	      <td><a href="https://github.com/AmineSoukara"><b>💸 test</b></a></td>
        <td><img alt="Stars" src="https://img.shields.io/github/stars/iampavangandhi/TradeByte?style=flat-square&labelColor=343b41"/></td>
        <td><img alt="Forks" src="https://img.shields.io/github/forks/iampavangandhi/TradeByte?style=flat-square&labelColor=343b41"/></td>
        <td><img alt="Language" src="https://img.shields.io/github/languages/top/iampavangandhi/TradeByte?label=javascript&style=flat-square"/></td>
      </tr>
      <tr>
	      <td><a href="https://github.com/DamienSoukara/FSub-Heroku"><b>👾 FSub</b></a></td>
        <td><img alt="Stars" src="https://img.shields.io/github/stars/iampavangandhi/TradeByte?style=flat-square&labelColor=343b41"/></td>
        <td><img alt="Forks" src="https://img.shields.io/github/forks/iampavangandhi/TradeByte?style=flat-square&labelColor=343b41"/></td>
        <td><img alt="Language" src="https://img.shields.io/github/languages/top/DamienSoukara/FSub-Heroku?style=flat-square&labelColor=000"/></td> 
      </tr>
      <tr>
	      <td><a href="https://github.com/AmineSoukara"><b>🤓 test</b></a></td>
        <td><img alt="Stars" src="https://img.shields.io/github/stars/iampavangandhi/iampavangandhi?style=flat-square&labelColor=000"/></td>
        <td><img alt="Forks" src="https://img.shields.io/github/forks/iampavangandhi/iampavangandhi?style=flat-square&labelColor=000"/></td>
        <td><img alt="Language" src="https://img.shields.io/badge/markdown-100%25-plue?style=flat-square&labelColor=000"/></td> 
      </tr>
    </tbody>
  </table>
  <br />
</details>
 
<details>	
  <br />
  <summary><b>⚙️ Things I use to get stuff done</b></summary>
  	<ul>
  	    <li><b>OS:</b> Ubuntu 20.04 </li>
	    <li><b>Laptop: </b> ... </li>
  	    <li><b>Browser: </b> Firefox Developer Edition </li>
	    <li><b>Terminal: </b> ... </li>
	    <li><b>Code Editor:</b> VSCode - The Best Editor Out There.</li>
	    <br />
	⚛️ Checkout My VSCode Configrations <a href="t.me">Here</a>.
	</ul>	
</details>


<hr>
<h2 align="center">©️ Damien Soukara</h2>
<p align="center"><b><i>"Боль неизбежна. Страдание необязательно"</i></b></p>
<p align="center">
<!--
<img src="https://metrics.lecoq.io/AmineSoukara?template=classic&isocalendar=1&languages=1&introduction=1&stars=1&people=1&followup=1&lines=1&activity=1&achievements=1&pagespeed=1&tweets=1&posts=1&stock=1&isocalendar.duration=half-year&languages.colors=github&languages.threshold=0%25&introduction.title=false&stars.limit=4&people.limit=28&people.size=28&people.types=followers%2C%20following&people.identicons=false&people.shuffle=false&activity.limit=5&activity.days=14&activity.filter=all&activity.visibility=all&activity.timestamps=false&achievements.threshold=C&achievements.secrets=false&achievements.limit=0&pagespeed.url=AmineSoukara.me&pagespeed.detailed=false&pagespeed.screenshot=false&tweets.attachments=false&tweets.limit=2&tweets.user=AmineSoukara&posts.descriptions=false&posts.covers=false&posts.limit=4&posts.user=.user.login&stock.duration=1d&stock.interval=5m&config.timezone=Africa%2FCasablanca">
-->
</p>

